import Page from "./page";

const TIMEOUT = 20000;

class LecturePage extends Page {

    get sidebar() { return $("div[class*='sidebar']") };
    get menuButton() { return $("label[class*='menu__button']") };
    get menuItems() { return $$("div[class='tabs__item'] label[class*='tabs__title']") };
    get menuSubItems() { return $$("a[class*='sub__item']") };
    get contentContainer() { return $("article[class*='lecture']") };
    get confirmButton() { return $("//input[contains(@class,'rating__button')]") };
    get avgMarkText() { return $("//p[contains(@class,'rating__text') and contains(text(),'Средняя оценка лекции')]") };
    get prevLink() {return $("//a[text()='Предыдущая лекция']")};
    get nextLink() {return $("//a[text()='Следующая лекция']")}
    getMenuSubItemsById(id) { return $$(`//div[@id='tab_0${id}']/a`) };
    getStar(mark) { return $(`//label[contains(@for,'star=${mark}')]`)};

    open() {
        super.open("/lecture");
    };

    getAvgMarkText() {
        this.avgMarkText.waitForDisplayed({ timeout: TIMEOUT });
        return this.avgMarkText.getText();
    }

    toLecture(lecture) {
        super.open(`lecture/${lecture}`);
        this.contentContainer.waitForDisplayed();
    }
    
    clickConfirmButton() {
        this.confirmButton.waitForClickable({ timeout: TIMEOUT });
        this.confirmButton.click();
    }

    clickRating(mark) {
        this.getStar(mark).waitForClickable({ timeout: TIMEOUT });
        this.getStar(mark).click();
    }

    clickToMenuItem(itemName) {
        $(`//label[@class='tabs__title' and ./span[text()='${itemName}']]`).click()
    }

    clickToMenuSubItem(subItemName) {
        $(`//div[@class='tabs__block']/a[text()='${subItemName}']`).click();
    }

    getMenuItemsNames() {
        let items = [];
        this.menuItems.forEach(element => {
            items.push(element.$("span").getText());
        });
        return items;
    };

    getMenuSubItemsNames(id) {
        this.getMenuSubItemsById(id)[0].waitForDisplayed();
        let items = [];
        for (let i = 0; i < this.getMenuSubItemsById(id).length; i++) {
            items.push(this.getMenuSubItemsById(id)[i].getText())
        }
        return items;
    };

    openMenu() {
        this.menuButton.waitForEnabled();
        this.menuButton.click();
        this.menuItems[0].waitForEnabled();
    }

    openMenuItem(itemName, id) {
        this.clickToMenuItem(itemName);
        this.getMenuSubItemsById(id)[0].waitForDisplayed()
    };


    isTextInTitle(text) {
        if ($(`//h1[text()[contains(., '${text}')]]`))
            return true;
    }
}

export default LecturePage;