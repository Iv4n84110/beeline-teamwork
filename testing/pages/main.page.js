import Page from "./page.js";
import LecturePage from "./lecture.page.js";

const TIMEOUT = 20000;

class MainPage extends Page {

    get LectureLink() { return $("//a[@href='/lecture']") };
    get searchLink() { return $("button[class*='nav__search']") };
    get searchSection() { return  $("input[class*='search__input']")};
    get searchResults() {return  $$("//a[contains(@class,'search__link')]")};
    get searchResult() {return  $("//a[contains(@class,'search__link')]")};

    getsearchResults() {
        this.searchResult.waitForDisplayed({ timeout: TIMEOUT });
        return  this.searchResults;
    }
    
    getSearchPlaceholder() { 
       this.searchSection.waitForDisplayed({ timeout: TIMEOUT });
       return  this.searchSection.getAttribute("placeholder");
    };

    open() {
        super.open();
    };

    toLectures() {
        this.LectureLink.click();
        let lecturePage = new LecturePage();
        return lecturePage;
    };

}

export default MainPage;