import MainPage from "../pages/main.page.js";

const assert = require("assert");
const { addStep } = require("@wdio/allure-reporter").default;
const { addEnvironment } = require('@wdio/allure-reporter').default
const fs = require('fs');

let mainPage = new MainPage();

let QUERIES = {
  'Александр': 'Лекция 11. Блочная верстка',
  'Лекция 5': 'Лекция 5. Автоматизация тестирования: Page Object Pattern',
  'лекции': 'Лекция 1. Тестирование, автотесты, Selenium Webdriver, html-теги, атрибуты'
}


const thresholdPosition = 5;

describe("Проверка поиска", () => {

  before(() => {
    addEnvironment('browserName: ', 'Chrome');
    addEnvironment('browserVersion: ', '80.0.3987.132');
    addEnvironment('OS: ', 'win10 x64');
    mainPage.open();
  });

  it("Переход в раздел поиска", () => {
    addStep("Нажать на кнопку поиска");
    mainPage.searchLink.click();

    addStep("Проверить, что в placeholder верное значение");
    const placeholder = mainPage.getSearchPlaceholder();
    assert.deepEqual(placeholder, 'Лекция...');
  });

  it("Поисковые запросы", () => {
    addStep("Последовательно ввести запросы");
    for (let querry in QUERIES) {      
      console.log(querry + " -> " + QUERIES[querry]);
      mainPage.searchSection.setValue(querry);
      let success = false;
      let results =  mainPage.getsearchResults();
      let max =  results.length > thresholdPosition ? thresholdPosition : results.length;
      for (let i = 0; i<max; i++) {
        let res = results[i];      
        if (res.getText().localeCompare(QUERIES[querry])) {    
          success=true; 
        }
      }
      if (success) {
          assert(true);
      }
      else {
          console.log("querry not passed: ", querry)
          assert (false);
      }      
    }
  });
});