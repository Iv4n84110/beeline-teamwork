import MainPage from "../pages/main.page.js";

const assert = require("assert");
const { addStep } = require("@wdio/allure-reporter").default;
const { addEnvironment } = require('@wdio/allure-reporter').default

let mainPage = new MainPage();
let lecturePage;

const NUM_LECTURES = 43;
const RATING = 3;

describe("Проверка рейтинга", () => {

  before(() => {
    addEnvironment('browserName: ', 'Chrome');
    addEnvironment('browserVersion: ', '80.0.3987.132');
    addEnvironment('OS: ', 'win10 x64');
    mainPage.open();
  });

  it("Переход на страницу с лекциями", () => {
    addStep("Нажать на ссылку \"Лекции\"");
    lecturePage = mainPage.toLectures();

    addStep("Проверить, что URL изменился на верный");
    assert.deepEqual(browser.getUrl(), `http://localhost:3000/lecture/1`);
  });

  it("Проверка рейтинга", () => {
    addStep("Последовательно идти по лекциям");
    for (let lecture = 1; lecture<NUM_LECTURES; lecture++) {
        lecturePage.toLecture(lecture);
        addStep("Кликнуть рейтинг");
        lecturePage.clickRating(RATING);
        addStep("Подтвердить");
        lecturePage.clickConfirmButton();
        let avgMarkText = lecturePage.getAvgMarkText();
        assert.deepEqual(true, avgMarkText.includes("Средняя оценка лекции"));
    }
  });
});