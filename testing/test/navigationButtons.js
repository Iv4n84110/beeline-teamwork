import MainPage from "../pages/main.page.js";

const assert = require("assert");
const { addStep } = require("@wdio/allure-reporter").default;
const { addEnvironment } = require('@wdio/allure-reporter').default

let mainPage = new MainPage();
let lecturePage;
const NUM_LECTURES = 43;


describe("Переход по лекциям", () => {

  before(() => {
    addEnvironment('browserName: ', 'Chrome');
    addEnvironment('browserVersion: ', '80.0.3987.132');
    addEnvironment('OS: ', 'win10 x64');
    mainPage.open();
  });

  it("Переход на страницу с лекциями", () => {
    addStep("Нажать на ссылку \"Лекции\"");
    lecturePage = mainPage.toLectures();

    addStep("Проверить, что URL изменился на верный");
    assert.deepEqual(browser.getUrl(), `http://localhost:3000/lecture/1`);
  });

  it("Проверить работу кнопок 'Следующая лекция'", () => {
    for (let i = 2; i < (NUM_LECTURES+1); i++) {
      addStep("Нажать на кнопку 'Следующая лекция' и дождаться загрузки страницы");
      lecturePage.nextLink.click();
      lecturePage.contentContainer.waitForDisplayed();
      addStep("Проверить, что url верный");
      assert.deepEqual(browser.getUrl(), `http://localhost:3000/lecture/${i}`);
    }
  })
  it("Проверить работу кнопок 'Предыдущая лекция'", () => {
    for (let i = (NUM_LECTURES-1); i > 0; i--) {
      addStep("Нажать на кнопку 'Предыдущая лекция' и дождаться загрузки страницы");
      lecturePage.prevLink.click();
      lecturePage.contentContainer.waitForDisplayed();
      addStep("Проверить, что url верный");
      assert.deepEqual(browser.getUrl(), `http://localhost:3000/lecture/${i}`);
    }
  })
});