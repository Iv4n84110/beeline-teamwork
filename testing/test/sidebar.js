import MainPage from "../pages/main.page.js";

const assert = require("assert");
const { addStep } = require("@wdio/allure-reporter").default;
const { addEnvironment } = require('@wdio/allure-reporter').default

let mainPage = new MainPage();
let lecturePage;

describe("Переход по лекциям", () => {

  before(() => {
    addEnvironment('browserName: ', 'Chrome');
    addEnvironment('browserVersion: ', '80.0.3987.132');
    addEnvironment('OS: ', 'win10 x64');
    mainPage.open();
  });

  it("Переход на страницу с лекциями", () => {
    addStep("Нажать на ссылку \"Лекции\"");
    lecturePage = mainPage.toLectures();

    addStep("Проверить, что URL изменился на верный");
    assert.deepEqual(browser.getUrl(), `http://localhost:3000/lecture/1`);
  });

  it("Переход по лекциям", () => {
    lecturePage.openMenu();
    let menuItems = lecturePage.getMenuItemsNames();

    for (let i = 0; i < menuItems.length; i++) {
      addStep(`открыть ${menuItems[i]}`);
      lecturePage.menuItems[i].scrollIntoView();
      lecturePage.openMenuItem(menuItems[i], i);
      const subItems = lecturePage.getMenuSubItemsNames(i);
      for (let k = 0; k < subItems.length; k++) {
        const element = subItems[k];
        addStep(`открыть ${element}`);
        lecturePage.clickToMenuSubItem(element);
        lecturePage.contentContainer.waitForDisplayed();
        assert.deepEqual(lecturePage.isTextInTitle(element), true, `ошибка при переходе на '${element}'`);
      }
      addStep(`закрыть ${menuItems[i]}`);
      lecturePage.clickToMenuItem(menuItems[i]);
    }

  })
});