import React from 'react';
import './styles.css'
import myimage5 from './img/telegram.png'
import myimage6 from './img/youtube_alt.png'
import { Link } from 'react-router-dom';


const FooterMenu = (props) => {
    return (
        <Link to={props.footer_link} className='footer__link footer__text'>
            <li className='footer__li'>{props.footer_component}</li>
        </Link>
    )
}
const Footer = () => {
    return (
        <footer className='footer'>
            <div className='footer__wrapper'>
                <div className='footer__block'>
                    <ul className='footer__list'>
                        <FooterMenu footer_component="На главную" footer_link="/" />
                        <FooterMenu footer_component="Лекции" footer_link="/lecture" />
                        <FooterMenu footer_component="О нас" footer_link="/about" />
                        {/*<FooterMenu footer_component="Общение" ="https://tlgg.ru/bee_fludd" />      Должен быть написан через a*/}
                    </ul>
                    <div className='bee'>
                        <a href="https://tlgg.ru/frontendzavtrak" className='last footer__text' target='_blank'><img src={myimage5} className="footer__img first" /></a>
                        <a href="https://www.youtube.com/channel/UC_mZP7jedeAK9bB3_CkoDjQ/videos" className='last footer__text' target='_blank'><img src={myimage6} className="footer__img second" /></a>
                    </div>
                </div>
            </div>
        </footer>

    )
}
export default Footer;
