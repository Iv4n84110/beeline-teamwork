import React from 'react';


import Header from '../Header';
import Footer from '../Footer';

import './styles.css';
import ScrollUpButton from "react-scroll-up-button"; 


const PageLayout = ({ children }) => {
  return (
    <div className="container">
        <Header />
        <ScrollUpButton style={{outline: "none"}} />
        <div className="content">
          {children}
        </div>
        <Footer />
    </div>
  );
}

export default PageLayout;