import React, { useState } from "react";

import { Item, Logo, Search } from "./components";

import "./styles.css";

const Header = () => {

  const [isSearching, setSearching] = useState(false);

 const handleClick = () => {
    setSearching(true)
  }

 const closeSearch = () => {
    setSearching(false);
  }

    return (
      <header className="header">
        <nav className="nav">
          <Logo />
          <ul className="nav__list">
            <Item role="lectures" link="/lecture" text="Лекции"></Item>
            <Item role="about" link="/about" text="О нас"></Item>
            <li className="nav__item">
              <button className="nav__search" onClick={handleClick} />
            </li>
          </ul>
        </nav>
        <Search isActive={isSearching} closeSearch={closeSearch}></Search>
      </header>
    );
}

export default Header;
