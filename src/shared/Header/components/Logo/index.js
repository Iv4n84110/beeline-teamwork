import React from 'react';
import { Link } from 'react-router-dom';

import "./styles.css";

const Logo = () => {

  const infinity = 'M93.9,46.4c9.3,9.5,13.8,17.9,23.5,17.9s17.5-7.8,17.5-17.5s-7.8-17.6-17.5-17.5c-9.7,0.1-13.3,7.2-22.1,17.1 c-8.9,8.8-15.7,17.9-25.4,17.9s-17.5-7.8-17.5-17.5s7.8-17.5,17.5-17.5S86.2,38.6,93.9,46.4z';
  return (
    <div className='logo'>
      <Link to='/' className='logo__link'>
        <p className='logo__text'>team</p>
        <div className='logo__img'>
          <svg viewBox="0 0 100 30" className='logo__svg'>
            <path className='path' fill="transparent" strokeWidth="5" d={infinity} />
            <path className='path1' fill="transparent" strokeWidth="5" d={infinity} />
          </svg>
        </div>
      </Link>
    </div>
  );
}

export default Logo;