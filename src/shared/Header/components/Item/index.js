import React from 'react';
import { Link } from 'react-router-dom';

import "./styles.css";

const Item = (props) => {
  return (
    <li className='nav__item'>
      <Link to={props.link} className='nav__link'>
        <div className={`nav__${props.role}`} />
        <p className='nav__text'>{props.text}</p>
      </Link>
    </li>
  )
}

export default Item;