import React, { useState, useEffect } from "react";
import Loader from 'react-loader-spinner';

import "./styles.css";

import Result from "./result";

const Search = (props) => {
  const [results, setResults] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [isSucsess, setSucsess] = useState(true);
  const [value, setValue] = useState('');

  useEffect(() => {
    let timerId = setTimeout(() => {
      search(value);
    }, 500);
    clearTimeout(timerId - 1);
  }, [value]);

  const isActive = props.isActive;

  const handleChange = (e) => {
    setValue(e.target.value);
  }

  const search = (value) => {
    setResults([]);
    setSucsess(true);
    setLoading(true);
    value = value.trim();
    if (value) {
      fetch(`/api/search/${value}`)
        .then((res) => res.json())
        .then((result) => {
          if (typeof result !== 'undefined' && result.length > 0) {
            for (let i of result) {
              setResults(prevState => [...prevState, {
                lectureNumber: i.lectureNumber,
                text: i.title,
                terms: i.terms
              }]);
              setLoading(false);
              setSucsess(true);
            }
          }
          else {
            setResults([]);
            setSucsess(false);
            setLoading(false);
          }
        })
    }
    else {
      setLoading(false);
      setSucsess(true);
    }
  }

  
  return (
    <div className="search__wrapper">
      {isActive && (
        <div className="search__overlay">
          <div className="search">
            <div className="search__inner">
              <input
                autoFocus
                type="text"
                className="search__input"
                placeholder="Лекция..."
                value={value}
                onChange={handleChange}
              />
              <button
                className="search__button"
                onClick={() => {
                  props.closeSearch();
                }}
              ></button>
              {isLoading && value ? (
                <Loader className='search__loader'
                  type="ThreeDots"
                  color="#e2127a"
                  height="20"
                  width="100%"
                />) : null
              }
              {results.length ? (
                <div className="search__results">
                  {results.map((result) => (
                    <Result
                      key={`search__${result.lectureNumber}`}
                      text={result.text}
                      lectureNumber={result.lectureNumber}
                      terms={result.terms}
                      closeSearch={props.closeSearch}
                    ></Result>
                  ))}
                </div>) : null}
              {isSucsess ? null : (
                <div className="search__noresults">
                  Ничего по вашему запросу
                </div>
              )}

            </div>
          </div>
        </div>
      )
      }
    </div >
  );
}
export default Search;
