import React from "react";
import { Link } from 'react-router-dom';

import "./styles.css";

const Result = (props) => {
  let terms = [];

  for (const [i, term] of props.terms.entries()) {
    if (i === props.terms.length - 1) {
      terms.push(term.toUpperCase())
    }
    else {
      terms.push(term.toUpperCase() + '; ')
    }
  }

  return (
    <div className="search__result">
      <Link
        className="search__link"
        to={`/lecture/${props.lectureNumber}`}
        onClick={() => { props.closeSearch(); }}
      >
        {props.text}
        <span className="search__terms" >{terms}</span>
      </Link>
    </div>
  );
}

export default Result;
