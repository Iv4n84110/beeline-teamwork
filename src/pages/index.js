import About from './About'
import Lecture from './Lecture'
import Main from './Main'
import Page404 from './Page404'


export {About, Lecture, Main, Page404};