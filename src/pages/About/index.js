import React from 'react';
import PageLayout from '../../shared';
import { Achives, Team } from "./components";
import './styles.css'

const About = () => {
  return (
    <PageLayout>
      <section className="about">
        <div className="about__wrapper">
          <div className="intro">

            <h1 className='about__title'>О нас</h1>
            <div className="intro-p">
              <p>Мы - команда начинающих front-end разработчиков. Выпусники прокекта Beeinterns. Данный ресурс
              наша первая совместная работа, которая делалась для получения диплома. Очень надеемся, что
                            все материалы, которые мы созбрали здесь будут вам полезны, и вы улучшите свои знания! </p>
            </div>
          </div>

          <h2 className='about__subtitle'><span className="subtitle">Команда</span></h2>
        </div>
        <Team />
        <Achives />
      </section>
    </PageLayout>
  )
}
export default About;