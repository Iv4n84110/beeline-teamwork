import React from 'react';
import "./styles.css"

import {massAchive} from '../../../../utils/constants'

const Achives = () => {

    const achivements = () => {
        let developers = [];
        for (let key in massAchive) {
            developers.push (
                <div key ={`developer__${key}`} className="achivment__name achivment">
                    <span className="dev">{key}:</span>
                    {massAchive[key].map(element => {
                        return (
                            <ul key ={`achive__${element}`} className="achivments">
                                <li  className="achivments__item">{element}</li>
                            </ul>
                        )
                    })}
                </div>
            )
        }
        return developers;

    }

    return (

        <div className="other">
            <p>{"Как и положено в командной работе, каждый из нас внес свой вклад по мере возможностей и уровня подготовки. Что бы все это видели, мы просто оставим это здесь =)"}</p>
            < div className="other__wrapper">
                <div className="developer__achivments">
                    {achivements()}
                </div>
            </div>


        </div>
    )
}



export default Achives;