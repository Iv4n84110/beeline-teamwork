import React from 'react';
import './styles.css';
import Developer from "./Developer";
import myimage1 from './Developer/img/1.png'
import myimage2 from './Developer/img/2.png'
import myimage3 from './Developer/img/3.png'


const Team = () => {
    return (
        <div className='team'>
            <Developer name='Ilya Polozov' role='Front-end dev 99 lvl' image={myimage2}/>
            <Developer name='Ivan Baylo' role='Team lead' image={myimage1} />
            <Developer name='Ilya Yaroslavzef' role='Jun front-end dev' image={myimage3} />
        </div>


    )
}
export default Team;