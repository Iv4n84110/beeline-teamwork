import React from "react";


const Developer = (props) => {
    return (


            <div className="developer--wrapper">
                <img src={props.image} alt="" className="developer__img"/>
                <div className="developer__block">
                    <div className="developer__name">{props.name}</div>
                    <div className="developer__role">{props.role}</div>
                </div>
            </div>



    )
}
export default Developer;