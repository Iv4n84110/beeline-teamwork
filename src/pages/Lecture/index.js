import React, { useState, useEffect } from "react";
import "./styles.css";
import { Sidebar, Content } from "./components";
import { Redirect } from 'react-router-dom';
import { Page404 } from '../index';
import PageLayout from '../../shared';
import { getCookie, setCookie } from '../../utils/cookies'
import { lastLectureNumber } from '../../utils/constants'

const Lecture = (props) => {
  const lastLectureCookie = 'lastLecture'
  const lectureNumber = props.match.params.lecture;
  const getLastLecture = getCookie(lastLectureCookie);

  const [items, setItems] = useState(null)
  useEffect(() => {
    fetch("/api/menu")
      .then((res) => res.json())
      .then((menuItems) => setItems(menuItems))
  }, [])

  return lectureNumber ?
    (lectureNumber > lastLectureNumber) ? (<Page404 />) :
      (<PageLayout>
        <main className="lecture__wrapper">
          <div className="lecture__inner">
            <Sidebar menuItems={items} />
            {setCookie(lastLectureCookie, lectureNumber)}
            <Content lectureNumber={lectureNumber} menuItems={items}>
            </Content>
          </div>
        </main>
      </PageLayout>) :
    (<Redirect to={`/lecture/${getLastLecture ? getLastLecture : 1}`} />)
}
export default Lecture;
