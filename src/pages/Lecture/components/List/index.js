import React from 'react';

import "./styles.css";

const List = (props) => {
  return (
    <ul className="lecture__list">
      {props.items.map((item, i) => {
        return (
          <li key={i} className="lecture__item">
            <a href={item.link}
              target="_blank"
              rel='noopener noreferrer'
              className='lecture__link'
              download={item.download}>
              {item.text}
            </a>
          </li>
        );
      })}
    </ul>
  )
}
export default List;
