import React from "react";
import { Link } from 'react-router-dom';

import {lastLectureNumber} from '../../../../utils/constants'

import "./styles.css";

const Navigation = (props) => {
  return (
    <div className={`lecture__nav ${props.lectureNumber > 1 ? null : 'justify-end'}`}>
      {props.lectureNumber > 1 ? (
        <Link className='lecture__nav-link' to={`/lecture/${props.lectureNumber - 1}`}>
          Предыдущая лекция
        </Link>
      ) : null}
      {props.lectureNumber < lastLectureNumber ? (
        <Link className={'lecture__nav-link'} to={`/lecture/${+props.lectureNumber + 1}`}>
          Следующая лекция
        </Link>
      ) : null}
    </div>
  )
}
export default Navigation;
