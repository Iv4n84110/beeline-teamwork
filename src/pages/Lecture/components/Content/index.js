import React, { useState, useEffect } from "react";

import "./styles.css";
import { Navigation, Rating } from "../index";
import { MainSection, MaterialsSection, LinksSection, HomeworkSection } from "./sections";

import { } from '../../../../utils/constants'


const Content = (props) => {
  const [lecture, setLecture] = useState({
    id: null,
    section: null,
    description: null,
    url: null,
    files: {},
    links: null,
    homework: {},
  });
  const [hasError, setError] = useState(false);

  const menuItems = props.menuItems;
  const lectureNumber = props.lectureNumber;

  useEffect(() => {
    fetch(`/api/lecture/${lectureNumber}`)
      .then((res) => res.json())
      .then((lecture) => setLecture(lecture))
      .catch(err => setError(true))
  }, [lectureNumber]);


  const getLectureTitle = () => {
    if (lecture.section && menuItems) {
      if (menuItems[lecture.section][lectureNumber]) {
        return (`Лекция ${lectureNumber}. ${lecture.section}
        ${menuItems[lecture.section][lectureNumber]}`)
      };
    } return (
      `Лекция ${lectureNumber}.`
    )
  }

  return (hasError) ? <h1>Что-то пошло не так.</h1> : (

    <article className="lecture">
      <MainSection
        description={lecture.description}
        lector={lecture.lector}
        src={lecture.src}
        getLectureTitle={getLectureTitle}
      />
      <MaterialsSection 
        files={lecture.files}
        lectureNumber={lectureNumber}
      />
      <LinksSection
        links={lecture.links}
      />
      <HomeworkSection 
        homework={lecture.homework}
      />
      <Rating lectureNumber={lectureNumber} />
      <Navigation lectureNumber={lectureNumber} />
    </article>
  );
}

export default Content;
