import React from "react";

import { List } from "../../../index";

const LinksSection = (props) => {
  return (
    Array.isArray(props.links) && props.links.length) ? (
      <section className="lecture__section">
        <h2 className="lecture__subtitle">Полезные ссылки</h2>
        <List items={props.links} />
      </section>
    ) : null
}

export default LinksSection

