import MainSection from "./MainSection";
import MaterialsSection from "./MaterialsSection";
import LinksSection from "./LinksSection";
import HomeworkSection from "./HomeworkSection";

import "./styles.css";

export {MainSection, MaterialsSection, LinksSection, HomeworkSection};