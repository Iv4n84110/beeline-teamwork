import React from "react";

const HomeworkSection = (props) => {
  return props.homework.task ? (
    <section className="lecture__section">
      <h2 className="lecture__subtitle">Домашнее задание</h2>
      <p className="lecture__text lecture__homework">
        {props.homework.task}
      </p>
      <p className="lecture__text">
        <span className="strong">
          Отправлять: {props.homework.send}
        </span>
      </p>
    </section>
  ) : null
}

export default HomeworkSection
