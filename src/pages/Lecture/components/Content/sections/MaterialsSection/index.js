import React from "react";

import { List } from "../../../index";

const MaterialsSection = (props) => {  
  const items = [];

  const presentation = props.files.presentation ? {
    link: `http://localhost:5000/presentations/lecture-${props.lectureNumber}.pdf`,
    text: "Презентация"
  } : null;

  const project = props.files.project ? {
    link: `http://localhost:5000/projects/lecture-${props.lectureNumber}.zip`,
    text: "Проект",
    download: "true"
  } : null;

  if (presentation) items.push(presentation);
  if (project) items.push(project);

  return (props.files.project || props.files.presentation) ? (
    <section className="lecture__section">
      <h2 className="lecture__subtitle">Материалы лекции</h2>
      <List items={items} />
    </section>
  ) : null

}
export default MaterialsSection

