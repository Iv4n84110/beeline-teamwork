import React from "react";

import "./styles.css";

const MainSection = (props) => {
  return (
    <section className="lecture__section">
      <h1 className="lecture__title">{props.getLectureTitle()}</h1>
      <p className="lecture__text">{props.description}</p>
      <p className="lecture__text">
        <span className="strong">
          Лектор: {props.lector}
        </span>
      </p>
      <iframe
        src={props.src}
        frameBorder="0"
        allow="autoplay; encrypted-media"
        allowFullScreen
        title="video"
        className="lecture__video"
      />
    </section>
  )
}
export default MainSection

