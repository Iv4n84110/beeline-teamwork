import React from 'react';
import { Link } from 'react-router-dom';
import './styles.css';


let prevMenuItem = document.createElement("div");

const SideBar = (props) => {
  const menuItems = props.menuItems
  let k = 0;
  let res = []

  for (var key in menuItems) {
    let subs = []
    for (let prop in menuItems[key]) {
      subs.push(
        <Link key={`sidevar__link__${menuItems[key][prop]}`}
          to={`/lecture/${prop}`}
          className="sub__item"
          onClick={handleClick}>
          {menuItems[key][prop]}
        </Link>
      )
    }

    let idItem = "tabs__item-" + k;
    let idSubItem = "tab_0" + k;
    res.push(
      <div className="tabs__item" key={k}>
        <input type="checkbox" id={idItem} value="" name='input' className="tabs__input" />
        <label htmlFor={idItem} className="tabs__title">
          <span className="tabs__inner">{key}</span>
        </label>
        <div id={idSubItem} key={k + 10} className="tabs__block">
          {subs}
        </div>
      </div>
    )
    k++;
  };
  function handleClick(e) {
    e = e || window.event;
    e.target.classList.add("pink");
    prevMenuItem.classList.remove("pink");
    prevMenuItem = e.target;
  }

  return (
    <div className="sidebar">
      <div className='menu__wrapper'>
        <input type='checkbox' id='menu' value="" name='menu' className="tabs__input" />
        <label htmlFor='menu' className="menu__button"><span className="menu__button--text">Меню</span></label>
        <div className="menu">
          {res}
        </div>
      </div>
    </div>
  );
}



export default SideBar;
