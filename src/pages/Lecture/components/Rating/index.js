import React, { useState, useEffect } from "react";

import "./styles.css";

const Rating = (props) => {
  const [hasError, setError] = useState(false);
  const [send, setSend] = useState(null);
  const [rating, setRating] = useState(null);
  const [isVoted, setVoted] = useState(null)

  const lectureNumber = props.lectureNumber;

  useEffect(() => {
    setSend(null);
    setVoted(null);
     fetch(`/api/rating/${lectureNumber}`)
      .then((res) => res.json())
      .then((rating) => { let a = (rating.sum / rating.votes).toFixed(2); setRating(a) })
      .catch(err => {
        setError(true);
        console.log('error loading lecture rating');
      })
  }, [lectureNumber, isVoted])

  const handleStar = (e) => {
    setSend(e.target.value)
  }

  const handleSubmit = () => {
    fetch(`/api/rating/${lectureNumber}/${send}`);
    localStorage.setItem(`${lectureNumber}`, true);
    setVoted(true);
  }

  const fullStars = () => {
    let items = [];
    for (let i = 1; i < 6; i++) {
      if (i - 1 < rating) {
        items.push(<div key={`rating__fullStars_${i}`} className={rating >= i ? 'full-star' : 'half-star'}></div>)
      }
    }
    return (
      <div className='stars__wrapper'>
        <div className='stars__inner'>
          <div className="stars__full">
            {items}
          </div>
        </div>
      </div>);
  }

  const emptyStars = () => {
    let items = [];
    for (let i = 1; i < 6; i++) {
      items.push(<div key={`rating__emptyStar_${i}`} className='empty-star'></div>);
    }
    return (
      <div className="stars__empty">
        {items}
      </div>);
  }

  const markingStars = () => {
    let items = [];
    for (let i = 5; i > 0; i--) {
      items.push(<input key={`rating__input__${lectureNumber}_${i}`} type="radio" id={`star=${i}`} name="rating" value={i} onClick={handleStar}></input>)
      items.push(<label key={`rating__label__${lectureNumber}_${i}`} htmlFor={`star=${i}`}></label>)
    }
    return (
      <div className="stars">
        {items}
      </div>);
  }

  return (hasError) ? null :
    (localStorage.getItem(lectureNumber) || isVoted) ?
      (<div className="rating" >
        <p className='rating__text'>
          {`Средняя оценка лекции: ${rating}`}
        </p>
        <div className="stars">
          {emptyStars()}
          {fullStars()}
        </div>
      </div>
      ) : (
        <div className="rating" >
          <p className='rating__text'>Оцените лекцию</p>
          <div className='rating__inner'>
            <div className="stars">
              {markingStars()}
            </div>
            <input className='rating__text rating__button'
              type='submit'
              value='Подтвердить'
              onClick={handleSubmit}
              disabled={!send} />
          </div>
        </div>
      )
}

export default Rating;