import React from 'react';

import ChessCSS from './ChessCss';
import ChessHtml from './ChessHtml';
import ChessJS from './ChessJs';
import Fade from 'react-reveal/Fade';
import './styles.css';

const ChessSection = () => {
  return (<section className="main__chess">
    <Fade left>
      <ChessHtml />
    </Fade>
    <Fade right>
      <ChessCSS />
    </Fade>
    <Fade left>
      <ChessJS />
    </Fade>
  </section>
  )
}

export default ChessSection
