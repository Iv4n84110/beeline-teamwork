import React from 'react';
import '../../../../styles.css';
import { TagHTML } from '../../../Tags';
import { Link } from 'react-router-dom';
import '../styles.css';

const ChessJS = () => {
  return (
    <div className="main__items">
        <div className="main__items--code">
            <div className="main__items--title">
            Ты научишься JavaScript
            </div>
            <div className="main__contents--code">
            <code>
                <span className="main__contents__blue">&lt;</span>
                <span className="main__contents__brown">button</span>
                <br></br>
                <span className="main__contents--orange code__indent-1">
                onclick={" "}
                </span>
                {"myFunction()"}
                <br />
                <span className="main__contents__blue">&gt;</span>
                <br />
                <span className="code__indent-1">{"Click Me!"}</span>
                <br></br>
                <TagHTML start={false} tag="button" />

                <br></br>
                <br></br>
                <TagHTML start={true} tag="script" />
                <br></br>
                <span className="main__contents--blue code__indent-1">
                function{" "}
                </span>
                {"myFunction() {"}
                <br></br>
                <span className="main__contents--blue code__indent-2">var </span>
                {"x = "}
                <span className="code__indent-2">{"secondFunc ()"}</span>
                <br></br>
                <span className="code__indent-2">{"x.style.fontSize = "}</span>
                <span className="main__contents--orange">"25px"</span>
                {";"}
                <br></br>
                <span className="code__indent-2">{"x.style.color = "}</span>
                <span className="main__contents--orange">"red"</span>
                {";"}
                <br></br>
                <span className="code__indent-1">{"}"}</span>
                <br></br>
                <TagHTML start={false} tag="script" />
            </code>
            </div>
        </div>
        <div className="main__items--link">
            <Link className="main__contents--button"
            to="/lecture/19"
            >
            Лекции по JavaScript
            </Link>
        </div>
        </div>
    )
}

export default ChessJS;
