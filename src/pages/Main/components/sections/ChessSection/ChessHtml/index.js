import React from 'react';
import '../../../../styles.css';
import { TagHTML } from '../../../Tags';
import { Link } from 'react-router-dom';
import '../styles.css';

const ChessHtml = () => {
  return (
    <div className="main__items">
        <div className="main__items--code">
            <div className="main__items--title">
            Ты научишься HTML
            </div>
            <div className="main__contents--code">
            <code>
                <TagHTML start={true} tag="html" />
                <br></br>
                <span className="code__indent-1">
                <TagHTML start={true} tag="title" />
                    Лекция по HTML
                    <TagHTML start={false} tag="title" />
                </span>
                <br></br>
                <span className="code__indent-1">
                <TagHTML start={true} tag="body" />
                </span>
                <br></br>
                <br></br>
                <span className="code__indent-2">
                <TagHTML start={true} tag="h1" />
                </span>
                Заголовок
                <TagHTML start={false} tag="h1" />
                <br></br>
                <span className="code__indent-2">
                <TagHTML start={true} tag="p" />
                </span>
                Параграф
                <TagHTML start={false} tag="p" />
                <br></br>
                <br></br>
                <span className="code__indent-1">
                <TagHTML start={false} tag="body" />
                </span>
                <br></br>
                <TagHTML start={false} tag="html" />
            </code>
            </div>
        </div>
        <div className="main__items--link">
            <Link className="main__contents--button"
            to="/lecture/9">
            Лекции по HTML
            </Link>
        </div>
        </div>
    )
}

export default ChessHtml;
