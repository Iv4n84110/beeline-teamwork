import React from 'react';
import '../../../../styles.css';
import { TagCSS } from '../../../Tags';
import { Link } from 'react-router-dom';
import '../styles.css';

const ChessCSS = () => {
  return (
    <div className="main__items">
        <div className="main__items--link">
            <Link className="main__contents--button"
            to="/lecture/10">
            Лекции по CSS
            </Link>
        </div>
        <div className="main__items--code">
            <div className="main__items--title">Ты научишься CSS</div>
            <div className="main__contents--code">
            <code>
                <TagCSS item="tag" text="body" /> {"{"} <br></br>
                <span className="code__indent-1">
                <TagCSS item="key" text="background-color" />:{" "}
                </span>
                <TagCSS item="value" text="lightblue" />;<br></br>
                {"}"}
                <br></br>
                <br></br>
                <TagCSS item="tag" text="h1" /> {"{"}
                <br></br>
                <span className="code__indent-1">
                <TagCSS item="key" text="color" />:{" "}
                </span>
                <TagCSS item="value" text="white" />;<br></br>
                <span className="code__indent-1">
                <TagCSS item="key" text="text-align" />:{" "}
                </span>
                <TagCSS item="value" text="center" />;<br></br>
                {"}"}
                <br></br>
                <br></br>
                <TagCSS item="tag" text="p" /> {"{"}
                <br></br>
                <span className="code__indent-1">
                <TagCSS item="key" text="font-family" />:{" "}
                </span>
                <TagCSS item="value" text="verdana" />;<br></br>
                <span className="code__indent-1">
                <TagCSS item="key" text="font-size" />:{" "}
                </span>
                <TagCSS item="value" text="20px" />;<br></br>
                {"}"}
            </code>
            </div>
        </div>
        </div>
    )
}

export default ChessCSS;
