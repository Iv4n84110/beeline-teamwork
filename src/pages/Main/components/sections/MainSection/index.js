import React from "react";
import { main } from '../../../../../utils/main-content'
import './styles.css';


const MainSection = () => {
  return (
    <section>
      <div className="beeinterns__logo main__items--text">
        <div className="main__logo">&lt;{main.mainTitle.banner1}/&gt;</div>
        <div className="main__logo">&lt;{main.mainTitle.banner2}/&gt;</div>
      </div>

      <div className="main__title main__items--text">
        <span className="main__highlight">{main.mainTitle.text.highlight}</span>{main.mainTitle.text.ordinary}
      </div>

      <div className="main__title--text main__items--text">{main.mainContent.block1.title}</div>

      <div className="main__content--text main__items--text">
        {main.mainContent.block1.content.ordinary1}<span className="main__highlight">{main.mainContent.block1.content.highlight1}</span>
        {main.mainContent.block1.content.ordinary2}<span className="main__highlight">{main.mainContent.block1.content.highlight2}</span>{main.mainContent.block1.content.ordinary3}
      </div>

      <div className="main__title--text main__items--text">{main.mainContent.block2.title}</div>
      <div className="main__content--text main__items--text">
        <span className="main__highlight">{main.mainContent.block2.content.highlight1}</span>{main.mainContent.block2.content.ordinary1}
      </div>

      <div className="main__title--text main__items--text">
        {main.mainContent.block3.title}
      </div>
      <div className="main__content--text main__items--text">
        {main.mainContent.block3.content.ordinary1}
      </div>
    </section>
  )
}

export default MainSection;