import React, { useEffect, useRef } from "react";
import { animated, useSpring } from "react-spring";

import './styles.css';

const ParallaxSection = () => {
  const calc = y => `translateY(${y * 0.07}px)`;
  const ref = useRef();
  const [{ offset }, set] = useSpring(() => ({ offset: 0 }));

  const handleScroll = () => {
    const posY = ref.current.getBoundingClientRect().top;
    const offset = window.pageYOffset - posY;
    set({ offset });
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });

  return (
    <section ref={ref}>
      <div className="main__parallax">
        <div className="parallax__text">Начни уже сейчас!</div>
        <animated.div
          className="parallax__layer"
          style={{
            transform: offset.interpolate(calc)
          }}
        />
      </div>
    </section>
  )
}

export default ParallaxSection;