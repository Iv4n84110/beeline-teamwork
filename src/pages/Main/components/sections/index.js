import MainSection from './MainSection'
import ParallaxSection from './ParallaxSection'
import ChessSection from './ChessSection';

export {MainSection, ParallaxSection, ChessSection}
