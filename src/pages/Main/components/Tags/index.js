import React from 'react';
import "./styles.css";

const TagHTML = (props) => {
  return (
    <code>
      <span className="main__contents__blue">&lt;</span>
      <span className="main__contents__brown">{props.tag}</span>
      <span className="main__contents__blue">{props.start || '/'}&gt;</span>
    </code>
  )
}


const TagCSS = (props) => {
  return (<code>
    {(() => {
      switch (props.item) {
        case "tag": return (<span className="main__contents__brown">{props.text}</span>)
        case "key": return (<span className="main__contents__orange">{props.text}</span>)
        case "value": return (<span className="main__contents__blue">{props.text}</span>)
        default: ;
      }
    })()}
  </code>)
}

export {
  TagHTML,
  TagCSS,
}
