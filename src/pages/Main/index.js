import React from "react";
import "./styles.css";
import PageLayout from '../../shared';

import { MainSection, ParallaxSection, ChessSection } from './components/sections'

const Main = () => {
  return (
    <PageLayout>
      <div className="main__page--component">
        <div className="main__contents--wrap">
          <MainSection />
          <ParallaxSection />
          <ChessSection />
        </div>
      </div>
    </PageLayout>
  );
}

export default Main;
