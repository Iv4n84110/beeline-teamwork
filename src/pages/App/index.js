import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Lecture, Main, About, Page404 } from '../index';

import "../../styles/global.css";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path="/lecture/:lecture?" component={Lecture} />
        <Route exact path="/about" component={About} />
        <Route component={Page404} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
