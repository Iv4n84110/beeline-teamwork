This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
# Начало
## NPM
Когда скачаешь проект, в папке проекта используй:
```
npm install
```
Запускай так:
```
npm run dev
```
Если выдаёт ошибку, что не может найти модуль, нужно скачать этот модуль с помощью:
```
npm install module_name
```
## Word2vec
Модуль поиска нашего приложения может использовать Word2vec, но на некоторых устройствах это вызывает следующую ошибку:
```
Assertion failed: new_time >= loop->time, file c:\ws\deps\uv\src\win\core.c, line 309
[1] Proxy error: Could not proxy request /api/search/%D0%B2%D1%8B from localhost:3000 to http://localhost:5000/.
[1] See https://nodejs.org/api/errors.html#errors_common_system_errors for more information (ECONNRESET).
```
Поэтому данный модуль отключен по умолчанию. Если же вы хотите его включить, то измените строку 9 в server/server.js на 
```
const W2V_USE = true;
```