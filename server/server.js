const express = require('express');
const path = require('path');
const fetch = require('node-fetch');
const MiniSearch = require('minisearch')
const fs = require('fs');

const W2V_MODEL = 'tayga_none_fasttextcbow_300_10_2019'
const W2V_FORMAT = 'json'
const W2V_USE = false;

const lectureDir = "./data/lectures/";

const app = express();
let lectureNumber;
let lectures = [];
fs.readdir(lectureDir, (err, files) => {
    lectureNumber = files.length;
    for (let i = 1; i < lectureNumber; i++) {
        let lecture = `./data/lectures/lecture-${i}.json`
        let data = fs.readFileSync(lecture, 'utf8');
        lectures.push(JSON.parse(data))
    }

    let miniSearch = new MiniSearch({
        fields: ['title', 'description', 'lector'], // fields to index for full-text search
        storeFields: ['title', 'lectureNumber'] // fields to return with search results
    })

    // Index all documents
    miniSearch.addAll(lectures)

    function compare(a, b) {

        if (a.score < b.score) {
            return 1;
        }
        if (a.score > b.score) {
            return -1;
        }

        if (a.id < b.id) {
            return -1;
        }
        if (a.id > b.id) {
            return 1;
        }
        return 0;
    }
    function W2v_api(req, res) {
        let results = [];

        if (req.indexOf(' ') >= 0) {
            req = req.substr(0, req.indexOf(' '));
        }

        const encodedURI = encodeURI(req);

        fetch('https://rusvectores.org/' + W2V_MODEL + '/' + encodedURI + '/api/' + W2V_FORMAT)
            .then((res) => res.json())
            .then((result) => {
                if (typeof result !== 'undefined') {
                    for (let model of Object.keys(result)) {
                        for (let querry of Object.keys(result[model])) {
                            for (let similar of Object.keys(result[model][querry])) {
                                results = miniSearch.search(similar, { prefix: true })
                                if (results.length > 0) {
                                    return results;
                                }
                            }
                        }
                    }
                }
            })
            .then(results => {
                if (results !== undefined) {
                    results.sort(compare);
                    res.send(results);
                }
                else {
                    res.send([])
                }
            })
    }
    app.use(express.static('public'));

    app.get('/api/rating/:lecture', (req, res) => {
        res.sendFile(path.resolve(__dirname, `./data/ratings/rating-${req.params.lecture}.json`));
    });

    app.get('/api/rating/:lecture/:rating', (req, res) => {
        let data = fs.readFileSync(`./data/ratings/rating-${req.params.lecture}.json`, 'utf8');
        let rating = JSON.parse(data);
        rating.sum = +rating.sum + +req.params.rating;
        console.log(req.params.rating)
        rating.votes++;
        let str = JSON.stringify(rating);
        fs.writeFileSync(`./data/ratings/rating-${req.params.lecture}.json`, str);
    });

    app.get('/api/lecture/:lecture', (req, res) => {

        res.sendFile(path.resolve(__dirname, `./data/lectures/lecture-${req.params.lecture}.json`))

    });

    app.get('/api/menu', (req, res) => {
        res.sendFile(path.resolve(__dirname, `./data/menu/menu.json`))
    })

    app.get('/api/search/:querry', (req, res) => {
        let results = miniSearch.search(req.params.querry, { prefix: true })

        if ((typeof results === 'undefined' || results.length === 0) && W2V_USE) {
            W2v_api(req.params.querry, res);
        }
        else {
            results.sort(compare);
            res.send(results);
        }
    });
});

const port = 5000;

app.listen(port, () => console.log(`Serverr started on port ${port}`));


